# GoogleSheet To Json Adapter
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v2.0.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202020.3.4f1/gray)

Depedencies
- "com.gtion.custominspector": "https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/gtion-custom-inspector.git"
- "com.gdc.json-tools" : "https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/gdc-json-tools.git"

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## Whats New V2.0.1
- Replaced Gtion Custom Inspector with NaughtyAttributes
- Bug Fix, adapter can't used directly
- Ignore SheetTable with prefix '!'

## What is it?
A package to make retrive data from "GoogleSheet To Json WebService" Easier

## feature!
- Load From Json WebService!
- Ignore SheetTable with prefix '!' (ex: !Editor , !Play)

## Getting Started
##### Convert Object to JSON & Saving
To be able use this feature you need to create an adapter first which inherit from BaseSheetToJsonAdapter

### Adapter Script
```cs
using Gtion.Plugin.SheetToJson;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Data_00", menuName = "Sample/Adapter", order = 1)]
public class AdapterSample : SheetToJsonAdapter<AdapterJsonModel>
{
	// this is override function which will run when each sheet rechived
    public override void ProcessResult(string currentPath, string tableName, List<HttpResponseSheet<Dictionary<string, string>>> sheetData, ref AdapterJsonModel sheetConfig)
    {
        BattleConditionType conditionType = BattleHelper.GetBattleConditionType(tableName);
        sheetConfig.ConditionType = conditionType;

		// do another thing
		// ...
    }
}
```
### data model Script
```cs
public class AdapterJsonModel : GS_BaseConfig
{
	// feel free to add some properties below

	
}

```

then, this option should be showed up

![image](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/googlesheet-to-json-adapter-unity/uploads/53586d1d1db24c2e8b0a0a2555b6fc30/image.png)

then, you need to put Service Url and Sheetkey

![image](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/googlesheet-to-json-adapter-unity/uploads/4f959bb856a58535b1338f01ec1ca8ee/image.png)

lastly, you need to click the Fetcth button and the scriptable object will automatically generated for you :D

![image](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/googlesheet-to-json-adapter-unity/uploads/8c874e6b20fba5448b51981054cc9914/image.png)

## Response HTTP

the data structure of `HttpResponseSheet<T>` is like this
```cs
[System.Serializable]
public class HttpResponseSheet<T> // for default T will be > Dictionary<string , string>
{
	// this is name of you sheet (sheet tab)
	public string sheet { get; set; }
	// this contains list of row
	public List<T> value { get; set; }
	
	// ex:
	// A   | B   | C
	// asd | 123 | Foo
	
	// Converted like this
	// [{ "A" : "asd" , "B" : "123" , "C" : "Foo" }]
}
```


## Contributor


| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 

## License

MIT

**Free Software, Hell Yeah!**

