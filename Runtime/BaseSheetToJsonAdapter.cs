using UnityEngine;
using NaughtyAttributes;
using System.Threading.Tasks;
using System;
using GDC.Innovation.Json;
using UnityEditor;
using System.Collections.Generic;

namespace Gtion.Plugin.SheetToJson
{
    [System.Serializable]
    public class BaseSheetToJsonRequestConfig
    {
        public string webServiceUrl = "https://sheet-to-json.herokuapp.com/";
        public string sheetId;
    }

    public abstract class SheetToJsonAdapter<T> : BaseSheetToJsonAdapter<T,Dictionary<string , string>> where T : GS_BaseConfig
    {
        public override abstract void ProcessResult(string currentPath, string tableName, List<HttpResponseSheet<Dictionary<string , string>>> sheetData, ref T sheetConfig);
    }

    public abstract class BaseSheetToJsonAdapter<T,K> : ScriptableObject where T : GS_BaseConfig
    {

        [Header("Data")]
        [SerializeField]
        public List<T> GSheetData = new List<T>();
        public IReadOnlyList<T> SheetData => GSheetData;


        [Header("Function")]
        [SerializeField]
        private BaseSheetToJsonRequestConfig requestConfig = new BaseSheetToJsonRequestConfig();

        [ReadOnly]
        public string Status = "Ready";

        [Tooltip("Activate this to turn off unwanted fetching")]
        public bool SafeLock = true;

        [SerializeField]
        private SheetAdapterProcess<T, K> process;

        [Button]
        protected void ResetStatus()
        {
            Status = "Ready";
        }

        [Button]
        protected virtual void FetchData()
        {
            if (SafeLock) return;
            if (Status != "Ready") return;
            if (process == null) process = new SheetAdapterProcess<T, K>(this);

            process.FetchData(requestConfig.webServiceUrl, requestConfig.sheetId);
        }

        public virtual void BeforeProcess() { }

        public abstract void ProcessResult(string currentPath, string tableName, List<HttpResponseSheet<K>> sheetData, ref T sheetConfig);

        public virtual void AfterProcess() { }

       
    }

    #region Editor Process


    [System.Serializable]
    public class SheetAdapterProcess<T,K> where T : GS_BaseConfig
    {

        private BaseSheetToJsonAdapter<T, K> BaseJsonToAdapter;

        public SheetAdapterProcess(BaseSheetToJsonAdapter<T,K> baseJsonToAdapter)
        {
            BaseJsonToAdapter = baseJsonToAdapter;
        }
#if UNITY_EDITOR
        public virtual async void FetchData(string webServiceUrl, string sheetId)
        {
            if (BaseJsonToAdapter.SafeLock) return;
            try
            {
                BaseJsonToAdapter.Status = "Requesting GoogleSheet File";
                string responseFile = await GetRequest($"{webServiceUrl}getList?spreadsheetId={sheetId}");
                HttpResponseFile fileData = GDCJson.ConvertStringJsonToObject<HttpResponseFile>(responseFile);

                BaseJsonToAdapter.BeforeProcess();

                List<T> temp = BaseJsonToAdapter.GSheetData;
                BaseJsonToAdapter.GSheetData = new List<T>();

                string title = fileData.title;


                string currentPath = AssetDatabase.GetAssetPath(BaseJsonToAdapter);
                if (!System.IO.Directory.Exists(Application.dataPath + System.IO.Path.GetDirectoryName(currentPath).TrimStart("Assets".ToCharArray()) + "/" + title))
                {
                    AssetDatabase.CreateFolder(System.IO.Path.GetDirectoryName(currentPath), title);
                }
                currentPath = currentPath.Replace($"/{BaseJsonToAdapter.name}.asset", string.Empty);
                currentPath = currentPath + "/" + title;

                for (int i = 0; i < fileData.value.Count; i++)
                {
                    string tableName = fileData.value[i];
                    //Ignore table with prefix '!'
                    if (tableName.Length == 0 || tableName[0] == '!') continue;

                    BaseJsonToAdapter.Status = $"Requesting Sheet {tableName}";
                    string responseSheet = await GetRequest($"{webServiceUrl}getSheets?spreadsheetId={sheetId}&requestData={tableName}");
                    List<HttpResponseSheet<K>> sheetData = GDCJson.ConvertStringJsonToObject<List<HttpResponseSheet<K>>>(responseSheet);

                    //Create Scriptable Object
                    T sheetConfig = ScriptableObject.CreateInstance<T>();
                    sheetConfig.Sheet = tableName;
                    sheetConfig.name = tableName;
                    BaseJsonToAdapter.ProcessResult(currentPath, tableName, sheetData, ref sheetConfig);

                    string savePath = currentPath + "/" + tableName + ".asset";
                    AssetDatabase.CreateAsset(sheetConfig, savePath);
                    AssetDatabase.SaveAssets();

                    BaseJsonToAdapter.GSheetData.Add(sheetConfig);

                    EditorUtility.FocusProjectWindow();
                    Selection.activeObject = BaseJsonToAdapter;
                }


                //cleaning
                temp = temp.FindAll(a => a != null && BaseJsonToAdapter.GSheetData.Find(item => item.Sheet == a.Sheet) == null);
                foreach (T gsheet in temp)
                {
                    AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(gsheet));
                }

                BaseJsonToAdapter.Status = "Ready";

                BaseJsonToAdapter.AfterProcess();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                BaseJsonToAdapter.Status = "Ready";
            }
            BaseJsonToAdapter.SafeLock = true;

            //autosave
            AssetDatabase.Refresh();
            EditorUtility.SetDirty(BaseJsonToAdapter);
            AssetDatabase.SaveAssets();
        }

        protected virtual async Task<string> GetRequest(string url)
        {
            var webClient = new System.Net.WebClient();
            string source = await webClient.DownloadStringTaskAsync(url);
            return source;
        }
#else
        public async void FetchData(string webServiceUrl, string sheetId)
        {
            Debug.Log("Sheet To Json");
        }
#endif
    }
    #endregion
}




