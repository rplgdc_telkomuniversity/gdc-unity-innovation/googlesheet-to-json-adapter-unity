using System.Collections.Generic;
using UnityEngine;

namespace Gtion.Plugin.SheetToJson
{
    [System.Serializable]
    public class HttpResponseFile
    {
        public string title { get; set; }
        public List<string> value { get; set; }
    }

    [System.Serializable]
    public class HttpResponseSheet<T>
    {
        public string sheet { get; set; }
        public List<T> value { get; set; }
    }

    public abstract class GS_BaseConfig : ScriptableObject
    {
        public string Sheet;
    }
}
